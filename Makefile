.PHONY: up down start stop restart build web-logs clean flush

up:
	docker-compose up -d
down:
	docker-compose down
start:
	docker-compose up -d
stop:
	docker-compose stop
restart:
	docker-compose restart
build:
	docker-compose build
logs:
	docker-compose -f logs
clean:
	docker-compose rm; docker network prune
flush:
	echo "flushall" | redis-cli -h compose_redis_1.local
