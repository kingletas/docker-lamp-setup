## Goal

The goal is simply to provision an environment quickly and efficiently using docker - this environment requires that you get a mysql, mongo and solr dump. There are various releases that you can use:

* release/lean: only uses Apache & MySQL
* release/redis: uses Apache, MySQL, Memcache & Redis
* release/full: uses all services

## Setup

* Clone this repo ```git clone https://kingletas@bitbucket.org/kingletas/docker-lamp-setup.git```
* Based on your sources names, update the .env file to match your sources
* Edit you local.xml file to use the domain for the services (see the .env file)
* Add the MySQL credentials from the etc/env/mysql.env file
* Place your backups under var/sources (don't decompress the mongo backup)
* Ensure the mysql dump (e.g magento.sql matches your configuration file under .env)
* Place your codebase (or clone it to) var/www/site
* run ```docker-compose up -d``` or ```make start```


## FOLDER Structure 

* etc: contains all of the configuration files
* var: contains all of the data files:
    * sources: where all of your services restoration data will be
    * lib: contains the services data
    * www: Your web files 
    * images: contains all of the Dockerfile to build images if they don't already exist
* etc/env: where all of the environment files will be
* bin: where all executable files will be

## Examples

## Recomendations

* Don't change the docker-compose.yml - rather use the .env file to change paths and values or each service .env file found under the env.d folder
* Use the Makefile for shortcuts
